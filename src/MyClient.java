/*
 * This snippet of coude is copied from this web-page:
 * https://www.javatpoint.com/socket-programming
 * and the full-credit goes to its author.
 */

// ########################### CLIENT SIDE ##################################
import java.io.*;
import java.net.*;

class MyClient {
	public static void main(String args[]) throws Exception {

		Socket s = new Socket("localhost", 4444);
		DataInputStream din = new DataInputStream(s.getInputStream());
		DataOutputStream dout = new DataOutputStream(s.getOutputStream());
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str1 = "", str2 = "";

		while (!str1.contains("*")) {
			//while (!str1.contains("Finished")) {
				str1 += br.readLine();
			//}
			dout.writeUTF(str1);
			dout.flush();
		//going to wait to read from pipeline
			str2 = din.readUTF();
			System.out.println("SERVER: " + str2);
		}
		dout.close();
		s.close();

	}
}
